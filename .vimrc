call pathogen#infect()
syntax on              
set bs=2
set t_Co=256
set autoindent
set ruler
set autowrite
set number
set ai sw=2 sts=2 et
set laststatus=2
if $TMUX == ''
  set clipboard+=unnamed
endif
set enc=utf-8
set gfn=Inconsolata:h12
set antialias
set hidden
set incsearch
set smartcase
set hlsearch
set noswapfile
set background=dark
colors molokai
let java_highlight_functions="style"
let java_allow_cpp_keywords=1
let g:syntastic_java_checkers=['javac']
let g:syntastic_python_checkers=['pyflakes']
let g:sytastic_html_checkers=[]
let syntastic_mode_map = { 'passive_filetypes': ['html'] }
let g:ctrlp_working_path_mode = ''
let g:ctrlp_clear_cache_on_exit = 0

filetype plugin indent on

runtime! macros/matchit.vim

set pastetoggle=<F3>
set mouse=a
"set lines=65 columns=115
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe,*.class,*.pyc,*.bin

let g:ctrlp_working_path_mode = ''
let g:ctrlp_clear_cache_on_exit = 0
let g:ctrlp_working_path_mode = 'ra'

let g:formatprg_java = "astyle"
let g:formatprg_args_java = "--mode=java --style=ansi -A2s3"
map <F2> :NERDTreeTabsToggle<CR>
imap <F3> :w <CR>
imap <F1> <Esc>

autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
autocmd VimEnter * NERDTreeToggle
autocmd BufEnter * silent! lcd %:p:h


set guioptions-=T
set guioptions-=m
set guioptions-=r

noremap ; :
inoremap jf <Esc>
noremap <C-h> :bprev<CR>
noremap <C-l> :bnext<CR> 
noremap <C-c> :bdelete<CR>
noremap <C-a> ggVG<CR>
noremap <F4> magg=G`a
noremap <C-j> <Down>
noremap <C-k> <Up>

set tags=~/.tags
set complete=.,w,b,u,t,i

function! JAVASET()
  "setlocal makeprg=if\ \[\ -f\ \"Makefile\"\ \];then\ make\ $*;else\ if\ \[\ -f\ \"makefile\"\ \];then\ make\ $*;else\ javac\ -g\ %;fi;fi
  setlocal makeprg=javac\ %
  setlocal errorformat=%f:%l:\ %m  
  set cindent
  set tw=0
  setlocal omnifunc=javacomplete#Complete 
  setlocal completefunc=javacomplete#CompleteParamsInfo
  inoremap <buffer> <C-X><C-U> <C-X><C-U><C-P> 
  inoremap <buffer> <C-S-Space> <C-X><C-U><C-P>
  map <buffer> <F9> :make<CR>
  map <buffer> <F5> :!echo %\|awk -F. '{print $1}'\|xargs "java" %:r<CR>
endfunction

function! CPPSET()
  "setlocal makeprg=if\ \[\ -f\ \"Makefile\"\ \];then\ make\ $*;else\ if\ \[\ -f\ \"makefile\"\ \];then\ make\ $*;else\ g++\ -O2\ -g\ -Wall\ -W\ -o\ %.bin\ %;fi;fi
  setlocal makeprg=g++\ -O2\ -g\ -Wall\ -Wno-unused\ -o\ %.bin\ %
  setlocal errorformat=%f:%l:\ %m  
  set cindent
  set tw=0
  set nowrap
  map <buffer> <F9> :make<CR>
  map <buffer> <F5> :!echo %\| awk -F. '{print $1}'\|xargs %:p.bin <CR>
endfunction

function! PYSET()
  set tw=0
  setlocal ai sw=2 sts=2 et
  set nowrap
  map <buffer> <F6> :%s /br.readline()/raw_input()/g <CR> :%s /br = open/#br = open/g <CR>
  map <buffer> <F7> :%s /raw_input()/br.readline()/g <CR> :%s /#br = open/br = open/g <CR>
  map <buffer> <F5> :!echo %\|awk -F. '{print $1}'\|xargs python % <CR>
endfunction

function! SCALASET()
  set nowrap
  set ts=2 sts=2 sw=2
  map <buffer> <F5> :!echo %\|awk -F. '{print $1}'\|xargs scala % <CR>
endfunction

function! RUBYSET()
  setlocal ai sw=2 sts=2 et
  set nowrap
  map <buffer> <F5> :!ruby %<CR>
endfunction

autocmd FileType java call JAVASET()
autocmd FileType cpp call CPPSET()
autocmd FileType c call CPPSET()
autocmd FileType python call PYSET()
autocmd FileType ruby call RUBYSET()
autocmd FileType scala call SCALASET()
au BufRead,BufNewFile *.blade.php set filetype=html

let NERDTreeIgnore=['\.jnlp', '\.app$', '\.jar$', '\.bak$', '\.pyc$', '\.class$','\.bin$', '\.out$']
