:if search ('<+FILE_NAME+>')
:  %s/<+FILE_NAME+>/\=expand('%:r')/g
:endif
:if search ('<+CURSOR+>')
:  normal! "_da>
:endif
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <cstring>

using namespace std;

#define all(v) v.begin(), v.end()
#define pb(v, e) v.push_back(e)
#define rall(v) v.rbegin, v.rend()
#define ll long long
#define sz(v) (int) v.size()
#define az(x) (sizeof(x) / sizeof(x[0]))
#define oo (int) 13e7
#define fill(v, e) memset(v, e, sizeof v)

int main() {
   freopen("<+FILE_NAME+>.in", "r", stdin);

   <+CURSOR+>   
}
