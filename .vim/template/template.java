:if search ('<+FILE_NAME+>')
:  %s/<+FILE_NAME+>/\=expand('%:r')/g
:endif
:if search ('<+CURSOR+>')
:  normal! "_da>
:endif
/*
PROG: <+FILE_NAME+>
LANG: JAVA
*/
import java.util.*;
import java.io.*;

public class <+FILE_NAME+> {

   private void solve() throws Exception {
      BufferedReader br = new BufferedReader(new FileReader("<+FILE_NAME+>.in"));
      PrintWriter pw = new PrintWriter("<+FILE_NAME+>.out");
      //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      <+CURSOR+>
      pw.close();
   }

   public static void main(String[] args) throws Exception {
      new <+FILE_NAME+>().solve();
   }

   static void debug(Object...o) {
      System.err.println(Arrays.deepToString(o));
   }

}
